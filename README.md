Code for **C-RPN**

---

Heng Fan and Haibin Ling, Siamese Cascaded Region Proposal Networks for Real-Time Visual Tracking, *IEEE Conference on Computer Vision and Pattern Recognition* (CVPR), 2019.

---

The implementation is available at [here](https://drive.google.com/open?id=1rhSYcQcQtHocXjiOTZ13vZedC61kbIVC).

(1) Download the zip file and unzip it to your computer.

(2) Download [Matconvnet](http://www.vlfeat.org/matconvnet/) to the root folder of code, and then compile it.

(3) Download the pre-trained [model](https://drive.google.com/file/d/1WbkYUCX5hNbDJ2jhn3cjQyO-7KLe4siK/view?usp=sharing), and put it into the ./ROOT_FOLDER/model/.

(4) Run the matlab script run_tracker.m.

